#!/usr/local/bin/bash
set -euo pipefail
IFS=$'\n\t'


make generate_file

if [ -z $PREFIX ]; then
  cp generate_file "$PREFIX/local/"
else
  cp generate_file "/usr/bin/local"
fi
