# Generate File

A command line tool to allow you do generate a file of `n` lines and `c` random characters per line.

## Usage

```shell
generate_file file_name [number_of_lines] [length_of_line]
```

The `file_name` is a required parameter whereas `number_of_lines` will default to `100` lines as will `length_of_line`.

### Examples

This will generate a file named `jules_verne.txt` with 250 rows and 80 characters per row

```bash
generate_file jules_verne.txt 250 80
```

## Installation

The install file is currently a work in progress, but this can be built easily and is cross platform compatiable.

Steps to manually install:

1. Clone the repository `git clone git@gitlab.com:snood1205/generate-file.git`
2. Make the file `make generate_file`
3. Copy to your bin path (on macs this will be `/usr/local/bin`)

